using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientSupplierMapper.Models;

namespace MvcApplication1.Controllers
{   
    public class SuppliersController : Controller
    {
        private ClientSupplierContext context = new ClientSupplierContext();

        //
        // GET: /Suppliers/

        public ViewResult Index()
        {
            return View(context.Suppliers.Include(supplier => supplier.Clients).ToList());
        }

        //
        // GET: /Suppliers/Details/5

        public ViewResult Details(int id)
        {
            Supplier supplier = context.Suppliers.Single(x => x.ID == id);
            return View(supplier);
        }

        //
        // GET: /Suppliers/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Suppliers/Create

        [HttpPost]
        public ActionResult Create(Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                context.Suppliers.Add(supplier);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(supplier);
        }
        
        //
        // GET: /Suppliers/Edit/5
 
        public ActionResult Edit(int id)
        {
            Supplier supplier = context.Suppliers.Single(x => x.ID == id);
            return View(supplier);
        }

        //
        // POST: /Suppliers/Edit/5

        [HttpPost]
        public ActionResult Edit(Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                context.Entry(supplier).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(supplier);
        }

        //
        // GET: /Suppliers/Delete/5
 
        public ActionResult Delete(int id)
        {
            Supplier supplier = context.Suppliers.Single(x => x.ID == id);
            return View(supplier);
        }

        //
        // POST: /Suppliers/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Supplier supplier = context.Suppliers.Single(x => x.ID == id);
            context.Suppliers.Remove(supplier);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientSupplierMapper.Models;

namespace MvcApplication1.Controllers
{   
    public class ClientSupplierMapsController : Controller
    {
        private ClientSupplierContext context = new ClientSupplierContext();

        //
        // GET: /ClientSupplierMaps/

        public ViewResult Index()
        {
            return View(context.ClientSupplierMaps.Include(clientsuppliermap => clientsuppliermap.Client).Include(clientsuppliermap => clientsuppliermap.Supplier).Include(clientsuppliermap => clientsuppliermap.Product).ToList());
        }

        //
        // GET: /ClientSupplierMaps/Details/5

        public ViewResult Details(int id)
        {
            ClientSupplierMap clientsuppliermap = context.ClientSupplierMaps.Single(x => x.Id == id);
            return View(clientsuppliermap);
        }

        //
        // GET: /ClientSupplierMaps/Create

        public ActionResult Create()
        {
            ViewBag.PossibleClients = context.Clients;
            ViewBag.PossibleSuppliers = context.Suppliers;
            ViewBag.PossibleProducts = context.Products;
            return View();
        } 

        //
        // POST: /ClientSupplierMaps/Create

        [HttpPost]
        public ActionResult Create(ClientSupplierMap clientsuppliermap)
        {
            if (ModelState.IsValid)
            {
                context.ClientSupplierMaps.Add(clientsuppliermap);
                context.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.PossibleClients = context.Clients;
            ViewBag.PossibleSuppliers = context.Suppliers;
            ViewBag.PossibleProducts = context.Products;
            return View(clientsuppliermap);
        }
        
        //
        // GET: /ClientSupplierMaps/Edit/5
 
        public ActionResult Edit(int id)
        {
            ClientSupplierMap clientsuppliermap = context.ClientSupplierMaps.Single(x => x.Id == id);
            ViewBag.PossibleClients = context.Clients;
            ViewBag.PossibleSuppliers = context.Suppliers;
            ViewBag.PossibleProducts = context.Products;
            return View(clientsuppliermap);
        }

        //
        // POST: /ClientSupplierMaps/Edit/5

        [HttpPost]
        public ActionResult Edit(ClientSupplierMap clientsuppliermap)
        {
            if (ModelState.IsValid)
            {
                context.Entry(clientsuppliermap).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PossibleClients = context.Clients;
            ViewBag.PossibleSuppliers = context.Suppliers;
            ViewBag.PossibleProducts = context.Products;
            return View(clientsuppliermap);
        }

        //
        // GET: /ClientSupplierMaps/Delete/5
 
        public ActionResult Delete(int id)
        {
            ClientSupplierMap clientsuppliermap = context.ClientSupplierMaps.Single(x => x.Id == id);
            return View(clientsuppliermap);
        }

        //
        // POST: /ClientSupplierMaps/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ClientSupplierMap clientsuppliermap = context.ClientSupplierMaps.Single(x => x.Id == id);
            context.ClientSupplierMaps.Remove(clientsuppliermap);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}